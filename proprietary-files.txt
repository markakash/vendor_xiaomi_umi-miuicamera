# All unpinned blobs below are extracted from V14.0.5.0.TJBMIXM

# System-files
-system/priv-app/MiuiCamera/MiuiCamera.apk;OVERRIDES=Snap,Camera2,GoogleCameraGo,Aperature
system/priv-app/MiuiCamera/lib/arm64/libcamera_algoup_jni.xiaomi.so
system/priv-app/MiuiCamera/lib/arm64/libcamera_mianode_jni.xiaomi.so
system/lib64/libcom.xiaomi.camera.mianodejni.so
system/lib64/libcom.xiaomi.camera.mianodejni.so
system/lib64/libcom.xiaomi.camera.requestutil.so
system/lib64/libDocumentProcess.so
system/lib64/libfenshen_snpe.so
system/lib64/libffmpeg.so
system/lib64/libjni_jpegutil_xiaomi.so
system/lib64/libmimoji_avatarengine.so
system/lib64/libmimoji_bokeh_845_video.so
system/lib64/libmimoji_jni.so
system/lib64/libmimoji_soundsupport.so
system/lib64/libmimoji_tracking.so
system/lib64/libmimoji_video2gif.so
system/lib64/libmulti-wakeup-engine.so
system/lib64/librecord_video.so
system/lib64/android.hardware.camera.common@1.0.so
system/lib64/libhidltransport.so
system/lib64/libmotion_photo.so
system/lib64/libmotion_photo_mace.so
system/lib64/vendor.xiaomi.hardware.campostproc@1.0.so

# Vendor-files
vendor/lib64/libmiai_deblur.so
vendor/lib64/libsupermoon.so
vendor/lib64/libmulticam_optical_zoom_control.so
vendor/lib64/libflaw.so
vendor/lib64/libarcsat.so
vendor/lib64/libarcmulticamsat.so
vendor/lib64/libalCFR.so
